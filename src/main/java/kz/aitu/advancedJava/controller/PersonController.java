package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Person;
import kz.aitu.advancedJava.services.PersonServicce;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


@RestController
public class PersonController {

    private final PersonServicce personServicce;

    public PersonController(PersonServicce personServicce) {
        this.personServicce = personServicce;
    }

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(personServicce.findAll());
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteById(@PathVariable long id) {
        personServicce.deleteById(id);
    }

    @PostMapping("/api/v2/users/")
    public void insertInto(@RequestBody Person person) {
        personServicce.inserInto(person);
    }
}
