package kz.aitu.advancedJava.services;

import kz.aitu.advancedJava.model.Person;
import kz.aitu.advancedJava.repository.PersonRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class PersonServicce {
    private final PersonRepository personRepository;

    public PersonServicce(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Iterable<Person> findAll(){
        return personRepository.findAll();
    }

    public void deleteById(long id){
        personRepository.deleteById(id);
    }

    public void inserInto(Person person){
        personRepository.save(person);
    }

}
